## md2nsld 11.0 
## Script to compute the Neutron Scattering Length Density from MD simulations
## Leonardo Darre <lrdarre@gmail.com>
## King's College London | Department of Chemistry | Dr. Domene's group

##Create package, namespace and all default global variables  
#package require math::linearalgebra
package provide md2nsld 11.0
namespace eval NSLD:: {
	#export functions
	namespace export nsldrun    ; #compute the nsld
	namespace export nsldgui    ; #pop up GUI
	namespace export AddEntry   ; #add entry for group selection
	namespace export Wrap       ; #wrap atoms into the cell
	namespace export Unwrap     ; #make whole broken molecules from wrap procedure
	namespace export w2d        ; #deuterate a user defined percentage of water
        namespace export w2dsel     ; #deuterate a user defined selection
	namespace export d2w        ; #reverse water deuteration
	namespace export d2wsel     ; #reverse user defined selection deuteration
	namespace export ref        ; #reflectivity calculation procedure
	#windows handles
	variable w                  ; #main window
	variable f                  ; #frame for NSLD/Volfr Tab
	variable f_ref              ; #frame for Reflectivity Tab
	#NSLD/VolFr Tab
	variable binsize 2.0        ; #input of binsize  
	variable mol                ; #molecule or chain or segid corresponding to the selected group
	variable numsel 5           ; #number of groups to compute NSLD
	variable grpname            ; #name of the group
#	variable selstr             ; #selection string for groups 
	variable centresel "name P" ; #selection string for centering the output plot
	variable firstframe 0       ; #first frame for computing the NSLD
	variable lastframe          ; #last frame for computing the NSLD
	variable stepsize 1         ; #number of simulation frames between consecutive NSLD calculation frames
	variable deutperc 0.0       ; #percentage of water molecules to deuterate
	variable deutsel            ; #Array containing the selection for deuteration
	variable amvsel
	#Reflectivity Tab
	variable layer_title        ; #Title for each added layer in manual reflectometry calculation
        variable SLD_layer          ; #SLD for each added layer in manual reflectometry calculation
        variable thick_layer        ; #Thickness each added layer in manual reflectometry calculation 
        variable solvf_layer        ; #Solv vol. fr. for  each added layer in manual reflectometry calculation
        variable rough_layer        ; #Roughness for each added layer in manual reflectometry calculation
	variable numlay   3         ; #Number of layers defined by default in Ref calculation (0,1 and n+1).
	variable Qmin 0.005         ; #Moment transfer (Q) minimum value
	variable Qmax 0.5           ; #Moment transfer (Q) maximum value
	variable Qngrid 500         ; #Moment transfer (Q) number of grid points
	variable SLD0 2.07          ; #SLD of layer 0
	variable SLD1 3.47          ; #SLD of layer 1
	variable SLD2 -0.56         ; #SLD of layer 2
	variable thick1 10.0        ; #Thickness of layer 1
	variable rough1 2.0         ; #Roughness of layer 1
	variable thick2 5.0         ; #Thickness of layer 2
        variable rough2 2.0         ; #Roughness of layer 2
	variable SLDlast -0.56      ; #SLD of layer n+1
	variable roughlast 0.0      ; #Roughness of layer n+1
	variable rough 0.2          ; #Roughness for all simulation layers
	variable bcgr 0.0           ; #background signal in experiment
	variable scl  1.0           ; #Scale factor
	variable solvf1 0.0         ; #Solvent volume fraction of layer 1
	variable solvf2 0.0         ; #Solvent volume fraction of layer 2
	variable simSLDlist         ; #List exported from nsldrun to ref proc
	variable thick              ; #Average binsize ~ thicknes of simulation layers to use in ref proc
        variable fronting 0         ; #Number of sim NSLD bins to discard from the left
        variable backing  0         ; #Number of sim NSLD bins to discard from the right
	variable man_q              ; #Flag for checkbutton controlling the manual input of Q range
        variable exp_q              ; #Flag for checkbutton controlling the experimental input of Q range
        variable expfile            ; #path of the file containing the experimental reflectivity
	variable SLDmemory          ; #Flag for checkbutton controlling the usage of NLSD in memory (New MD-NSLD)
	variable SLDload            ; #Flag for checkbutton controlling the loading of NLSD save to disk (load MD-NSLD)
	variable savedNSLDfile      ; #path of the file containing the NSLD saved to disk (load MD-NSLD)

	for {set i 0} {$i<$numsel} {incr i} {
		set mol($i) ""
		set grpname($i) ""
#		set selstr($i) ""
	}
}

#add entry in NSLS/VolFr Tab
proc ::NSLD::AddEntry {} {
	variable w
	variable f
	variable mol
	variable grpname
#	variable selstr
	variable numsel

	entry $f.m$numsel -textvariable ::NSLD::mol($numsel) -state normal
        entry $f.n$numsel -textvariable ::NSLD::grpname($numsel) -state normal
#        entry $f.s$numsel -textvariable ::NSLD::selstr($numsel) -state normal
        entry $f.d$numsel -textvariable ::NSLD::deutsel($numsel) -state normal
	entry $f.amv$numsel -textvariable ::NSLD::amvsel($numsel) -state normal
        pack $f.m$numsel -side left
        pack $f.n$numsel -side left
#        pack $f.s$numsel -side left
        pack $f.d$numsel -side left
	pack $f.amv$numsel -side left
        grid configure $f.m$numsel -row [expr $numsel + 8] -column 0 -columnspan 2 -sticky "snew"
        grid configure $f.n$numsel -row [expr $numsel + 8] -column 2 -columnspan 1 -sticky "snew"
 #       grid configure $f.s$numsel -row [expr $numsel + 8] -column 4 -columnspan 2 -sticky "snew"
        grid configure $f.d$numsel -row [expr $numsel + 8] -column 3 -columnspan 2 -sticky "snew"
	grid configure $f.amv$numsel -row [expr $numsel + 8] -column 5 -columnspan 2 -sticky "snew"
        grid rowconfigure $f [expr $numsel + 8] -weight 0
	set numsel [expr $numsel + 1]
}

#add entry in Reflectivity Tab
proc ::NSLD::AddEntryRef {} {
        variable w
        variable f_ref
        variable layer_title
        variable SLD_layer
        variable thick_layer
        variable solvf_layer
	variable rough_layer
	variable numlay 
	
	set lay [expr $numlay]
	label $f_ref.l$lay -text "Layer $lay:  "
	grid configure $f_ref.l$lay -row [expr $numlay + 9] -column 0 -sticky "snew"	
        entry $f_ref.l($lay)_sld -textvariable ::NSLD::SLD($lay) -state normal
        pack $f_ref.l($lay)_sld -side left
        grid configure $f_ref.l($lay)_sld -row [expr $numlay + 9] -column 1 -columnspan 1 -sticky "snew"
	entry $f_ref.l($lay)_thick -textvariable ::NSLD::THICK($lay) -state normal
        pack $f_ref.l($lay)_thick -side left
        grid configure $f_ref.l($lay)_thick -row [expr $numlay + 9] -column 2 -columnspan 1 -sticky "snew"
	entry $f_ref.l($lay)_solvf -textvariable ::NSLD::SOLVF($lay) -state normal
        pack $f_ref.l($lay)_solvf -side left
        grid configure $f_ref.l($lay)_solvf -row [expr $numlay + 9] -column 3 -columnspan 1 -sticky "snew"
	entry $f_ref.l($lay)_rough -textvariable ::NSLD::ROUGH($lay) -state normal
        pack $f_ref.l($lay)_rough -side left
        grid configure $f_ref.l($lay)_rough -row [expr $numlay + 9] -column 4 -columnspan 1 -sticky "snew"
        grid rowconfigure $f_ref [expr $numlay + 9] -weight 0
        set numlay [expr $numlay + 1]
}

#Wrap atoms in the cell
proc ::NSLD::Wrap {} {
	variable centresel
	puts "WRAPPING ATOMS INTO THE CELL ..."
	#pbc wrap -molid top -all -center bb
	pbc wrap -molid top -all -center com -centersel $centresel
}

#Correct broken molecules caused by the wrapping procedure
proc ::NSLD::Unwrap {} {
	puts "MAKING MOLECULES WHOLE ...0"
	pbc join res -first 0 -last 0
	pbc unwrap -all
	pbc wrap -compound res -all -center com
}

#Randomly substitute water hydrogens by deuterium up to a user defined percentage
proc ::NSLD::w2d {deutperc} {
	variable finegrain
        variable coarsegrain 
	if { $finegrain } {
        	set wat_inx_list [[atomselect top "water and name OH2"] get index]
	} elseif { $coarsegrain } {
		set wat_inx_list [[atomselect top "resname WAT"] get index]
	}
        set wat_inx_temp $wat_inx_list
        set numwat [llength $wat_inx_list]
        set numdeu [expr floor($numwat*($deutperc/100.0))]
        set sel_list {}
        for {set i 0} {$i<$numdeu} {incr i} {
                set new_selinx [expr {int(rand()*[expr $numwat-1])}]
                lappend sel_list [lindex $wat_inx_temp $new_selinx]
                set wat_inx_temp [lreplace $wat_inx_temp $new_selinx $new_selinx]
                set numwat [llength $wat_inx_temp]
        }
        foreach seldeu $sel_list {
                set rue [[atomselect top "index $seldeu"] get residue]
		if { $finegrain } {
                	set selH1 [atomselect top "residue $rue and name H1"]
                	set selH2 [atomselect top "residue $rue and name H2"]
                	$selH1 set name DEU1
                	$selH1 set type DEUT
                	$selH2 set name DEU2
                	$selH2 set type DEUT
			$selH1 delete
                	$selH2 delete
		} elseif { $coarsegrain } {
			set selW [atomselect top "residue $rue and name W"]
			$selW set name WD
			$selW set type WD
			$selW delete
		}
        }
        set selall [atomselect top all]
        $selall writepdb deut.pdb
        $selall delete
}

#Group specific deuteration
proc ::NSLD::w2dsel {seldeut moldeut} {

        set h_inx [atomselect top "$moldeut and $seldeut and not noh"] 
	$h_inx set type DEU
	$h_inx set name DEU3
	$h_inx writepdb deut_sel.pdb
 	$h_inx delete
}

#Reverse water deuteration
proc ::::NSLD::d2w {} {
        variable finegrain
        variable coarsegrain

	if { $finegrain } {
		set selDEU1 [atomselect top "water and name DEU1"]
		set selDEU2 [atomselect top "water and name DEU2"]
		$selDEU1 set name H1
		$selDEU2 set name H2
		$selDEU1 set type HT
		$selDEU2 set type HT
		$selDEU1 delete
		$selDEU2 delete
	} elseif { $coarsegrain } {
		set selW [atomselect top "resname WAT and name WD"]
		$selW set name W
		$selW set type W
		$selW delete
	}
}

#Reverse deuteration for selections
proc ::::NSLD::d2wsel {} {

	set selDEU [atomselect top "type DEU"]
	$selDEU set name H0
	$selDEU set type H
	$selDEU delete
}

#create main window
proc ::NSLD::nsldgui {} {
	variable w
	variable f
	variable numsel 5	
	variable binsize
	variable mol
	variable grpname
	variable amvsel
#	variable selstr
	variable centresel
	variable firstframe
	variable lastframe
	variable stepsize
	variable calcnsld
	variable calcvolfr
	variable calcvolfr_perbin
	variable finegrain
	variable coarsegrain
        variable f_ref
        variable layer_title
        variable SLD_layer
        variable thick_layer
        variable solvf_layer
        variable rough_layer
	variable numlay
	variable sim_sld
	variable man_sld
        variable fronting
        variable backing
        variable deutsel
	variable man_q
	variable exp_q
	variable expfile
        variable SLDmemory 
        variable SLDload
        variable savedNSLDfile 
	
	#Main window frame
	set w .nsldgui
	toplevel $w
	wm title $w "NeutronRefTools"
	wm minsize $w 300 200

	#Build a high level frame (hlf) inside the main window to contain de notebook
	ttk::frame $w.hlf
	grid $w.hlf -column 0 -row 0 -sticky "nsew"
	# allow hlf to resize with window
	grid columnconfigure $w.hlf 0 -weight 1
	grid rowconfigure $w.hlf 0 -weight 1

	#Build notebook(nb)
	#It will contain tabs for (i) NSLD and Volume Fr. (ii) Reflectivity
	ttk::notebook $w.hlf.nb
	grid $w.hlf.nb -column 0 -row 0 -sticky "nsew"

	#Build NSLD/Volume Fr. Tab
	# build the frame, add it to the notebook
	ttk::frame $w.hlf.nb.nsld_vol -width 250 -height 250
	$w.hlf.nb add $w.hlf.nb.nsld_vol -text "NSLD/Volume Fraction"
	# allow frame to change width with window
	#grid columnconfigure $w.hlf.nb.nsld_vol 0 -weight 1

	#Shortcut for tab
	set f $w.hlf.nb.nsld_vol	

	#main frame
#	set f $w.frame
#	frame $f -bd 2 -relief raised 
#	pack $f -side top -fill x
#	grid configure $w.frame -row 1 -column 0 -sticky "snew"
#	grid rowconfigure $w 0 -weight 0
#	grid rowconfigure $w 1 -weight 1 -minsize 200
#	grid columnconfigure $w 0 -weight 1 -minsize 200

	#Configure the NSLD/Volume Fraction Tab options
	#entries for the list of groups
	label $f.m -text "Atom Selection:"
	label $f.n -text "Group Name:"
#	label $f.s -text "Atom Selection:"
        label $f.d -text "Deuterated Selection:"
	label $f.b -text "Bin Size:"
	label $f.c -text "Centering Group:"
	label $f.ti -text "First Frame:"
	label $f.tf -text "Last Frame:"
	label $f.ss -text "Step Size:"
	label $f.deu -text "Deuterated Water (%):"
	label $f.amv -text "Absolute Volume"
	grid configure $f.b -row 6 -column 0 -sticky "snew"
	grid configure $f.c -row 2 -column 0 -sticky "snew"
	grid configure $f.ti -row 3 -column 0 -sticky "snew"
	grid configure $f.tf -row 3 -column 2 -sticky "snew"
	grid configure $f.ss -row 3 -column 4 -sticky "snew"
	grid configure $f.m -row 7 -column 0 -columnspan 2 -sticky "snew"
	grid configure $f.n -row 7 -column 2 -columnspan 1 -sticky "snew"
#	grid configure $f.s -row 7 -column 4 -columnspan 2 -sticky "snew"
        grid configure $f.d -row 7 -column 3 -columnspan 2 -sticky "snew"
	grid configure $f.deu -row 6 -column 4 -columnspan 1 -sticky "snew"
	grid configure $f.amv -row 7 -column 5 -columnspan 1 -sticky "snew"
	entry $f.bin -textvariable ::NSLD::binsize -state normal 
	pack $f.bin -side left
	grid configure $f.bin -row 6 -column 1 -sticky "snew"
	entry $f.cen -textvariable ::NSLD::centresel -state normal
	pack $f.cen -side left
	grid configure $f.cen -row 2 -column 1 -columnspan 3 -sticky "snew"
	entry $f.fframe -textvariable ::NSLD::firstframe -state normal
	pack $f.fframe -side left
	grid configure $f.fframe -row 3 -column 1 -sticky "snew"
	entry $f.lframe -textvariable ::NSLD::lastframe -state normal
        pack $f.lframe -side left
        grid configure $f.lframe -row 3 -column 3 -sticky "snew"
	entry $f.stepsz -textvariable ::NSLD::stepsize -state normal
        pack $f.stepsz -side left
        grid configure $f.stepsz -row 3 -column 5 -sticky "snew"
	entry $f.deu_p -textvariable ::NSLD::deutperc -state normal
        pack $f.deu_p -side left
        grid configure $f.deu_p -row 6 -column 5 -sticky "snew"
	grid rowconfigure $f 0 -weight 0 -minsize 10	
	for {set i 0} {$i<$numsel} {incr i} {
		entry $f.m$i -textvariable ::NSLD::mol($i) -state normal
		entry $f.n$i -textvariable ::NSLD::grpname($i) -state normal
#		entry $f.s$i -textvariable ::NSLD::selstr($i) -state normal
                entry $f.d$i -textvariable ::NSLD::deutsel($i) -state normal
		entry $f.amv$i -textvariable ::NSLD::amvsel($i) -state normal
		pack $f.m$i -side left
		pack $f.n$i -side left
#		pack $f.s$i -side left
                pack $f.d$i -side left
		pack $f.amv$i -side left
		grid configure $f.m$i -row [expr $i + 8] -column 0 -columnspan 2 -sticky "snew"
		grid configure $f.n$i -row [expr $i + 8] -column 2 -columnspan 1 -sticky "snew"
#		grid configure $f.s$i -row [expr $i + 8] -column 4 -columnspan 2 -sticky "snew"
                grid configure $f.d$i -row [expr $i + 8] -column 3 -columnspan 2 -sticky "snew"
		grid configure $f.amv$i -row [expr $i + 8] -column 5 -columnspan 2 -sticky "snew"
		grid rowconfigure $f [expr $i + 8] -weight 0
        }	
	button $f.addentry -text "Add Entry" -command [namespace code AddEntry]
	pack $f.addentry -side left
	grid configure $f.addentry -row 0 -rowspan 2 -column 0 -sticky "snew"
	button $f.w -text "Wrap" -command [namespace code Wrap]
	pack $f.w -side left
	grid configure $f.w -row 0 -rowspan 2 -column 1 -sticky "snew"
	button $f.u -text "Join" -command [namespace code Unwrap]
        pack $f.u -side left
        grid configure $f.u -row 0 -rowspan 2 -column 2 -sticky "snew"
	button $f.but -text "RUN" -command [namespace code nsldrun]
	pack $f.but -side right
	grid configure $f.but -row 0 -rowspan 3 -column 5 -sticky "snew"
#	button $f.v -text "Component Volume" -command [namespace code compvol]
#       pack $f.v -side right
#	grid configure $f.v -row 3 -column 1 -sticky "snew"
	checkbutton $f.ns -width 15 -text { NSLD                  } -variable [namespace current]::calcnsld
	pack $f.ns -side left
	grid configure $f.ns -row 0 -column 4 -sticky "snew"
	checkbutton $f.vf -width 15 -text { Vol. Frac.(fast)    } -variable [namespace current]::calcvolfr
        pack $f.vf -side left
        grid configure $f.vf -row 1 -column 4 -sticky "snew"
	checkbutton $f.fg -width 15 -text { All Atoms              } -variable [namespace current]::finegrain
        pack $f.fg -side right
        grid configure $f.fg -row 0 -column 3 -sticky "snew"
	checkbutton $f.cg -width 15 -text { Coarse Grain        } -variable [namespace current]::coarsegrain
        pack $f.cg -side left
        grid configure $f.cg -row 1 -column 3 -sticky "snew"
	checkbutton $f.vfpb -width 15 -text { Vol. Frac.(slow)   } -variable [namespace current]::calcvolfr_perbin
        pack $f.vfpb -side left
        grid configure $f.vfpb -row 2 -column 4 -sticky "snew"

	#Build Reflectivity Tab
        # build the frame, add it to the notebook
        ttk::frame $w.hlf.nb.ref -width 250 -height 250
        $w.hlf.nb add $w.hlf.nb.ref -text "Reflectivity"
        # allow frame to change width with window
        grid columnconfigure $w.hlf.nb.ref 0 -weight 1

        #Shortcut for tab
        set f_ref $w.hlf.nb.ref
	
	#Configure the Reflectivity Tab options
	button $f_ref.addentry -text "Add Entry" -command [namespace code AddEntryRef]
        pack $f_ref.addentry -side left
        grid configure $f_ref.addentry -row 0 -column 0 -sticky "snew"
	button $f_ref.ref -text "Reflectivity" -command [namespace code ref]
        pack $f_ref.ref -side left
        grid configure $f_ref.ref -row 1 -column 0 -sticky "snew"
	checkbutton $f_ref.simsld -width 15 -text { MDsim NSLD } -variable [namespace current]::sim_sld
        pack $f_ref.simsld -side left
        grid configure $f_ref.simsld -row 2 -column 0 -sticky "snew"
	checkbutton $f_ref.mansld -width 15 -text { Manual NSLD } -variable [namespace current]::man_sld
        pack $f_ref.mansld -side left
        grid configure $f_ref.mansld -row 3 -column 0 -sticky "snew"	
	checkbutton $f_ref.manQ -width 15 -text { Manual Q            } -variable [namespace current]::man_q
        pack $f_ref.manQ -side left
        grid configure $f_ref.manQ -row 1 -column 1 -sticky "snew"
	checkbutton $f_ref.expQ -width 15 -text { Exp. Q               } -variable [namespace current]::exp_q
        pack $f_ref.expQ -side left
        grid configure $f_ref.expQ -row 1 -column 2 -sticky "snew"
	entry $f_ref.expQpath -textvariable ::NSLD::expfile -state normal
        pack $f_ref.expQpath -side left
        grid configure $f_ref.expQpath -row 1 -column 3 -columnspan 2 -sticky "snew"

	checkbutton $f_ref.newMDnsld -width 15 -text { New MD-NSLD  } -variable [namespace current]::SLDmemory
        pack $f_ref.newMDnsld -side left
        grid configure $f_ref.newMDnsld -row 0 -column 1 -sticky "snew"
        checkbutton $f_ref.loadMDnsld -width 15 -text { Load MD-NLSD } -variable [namespace current]::SLDload
        pack $f_ref.loadMDnsld -side left
        grid configure $f_ref.loadMDnsld -row 0 -column 2 -sticky "snew"
        entry $f_ref.loadMDnsldpath -textvariable ::NSLD::savedNSLDfile -state normal
        pack $f_ref.loadMDnsldpath -side left
        grid configure $f_ref.loadMDnsldpath -row 0 -column 3 -columnspan 2 -sticky "snew"

	label $f_ref.qm -text "Qmin:"
	label $f_ref.qM -text "Qmax:"
	label $f_ref.qng -text "Qgrid:"
	label $f_ref.frontt -text "Sim. Fronting:"
	label $f_ref.backt -text "Sim. Backing:"
	grid configure $f_ref.qm -row 2 -column 1 -sticky "snew"
	grid configure $f_ref.qM -row 3 -column 1 -sticky "snew"
	grid configure $f_ref.qng -row 4 -column 1 -sticky "snew"
	grid configure $f_ref.frontt -row 5 -column 1 -sticky "snew"
	grid configure $f_ref.backt -row 5 -column 3 -sticky "snew"
	entry $f_ref.qmt -textvariable ::NSLD::Qmin -state normal
        pack $f_ref.qmt -side left
        grid configure $f_ref.qmt -row 2 -column 2 -sticky "snew"
	entry $f_ref.qMt -textvariable ::NSLD::Qmax -state normal
        pack $f_ref.qMt -side left
        grid configure $f_ref.qMt -row 3 -column 2 -sticky "snew"
	entry $f_ref.qngt -textvariable ::NSLD::Qngrid -state normal
        pack $f_ref.qngt -side left
        grid configure $f_ref.qngt -row 4 -column 2 -sticky "snew"
	entry $f_ref.front -textvariable ::NSLD::fronting -state normal
        pack $f_ref.front -side left
        grid configure $f_ref.front -row 5 -column 2 -sticky "snew"
	entry $f_ref.back -textvariable ::NSLD::backing -state normal
        pack $f_ref.back -side left
        grid configure $f_ref.back -row 5 -column 4 -sticky "snew"
	label $f_ref.bcgr -text "Background:"	
	grid configure $f_ref.bcgr -row 2 -column 3 -sticky "snew"
	entry $f_ref.bcgrt -textvariable ::NSLD::bcgr -state normal
        pack $f_ref.bcgrt -side left
        grid configure $f_ref.bcgrt -row 2 -column 4 -sticky "snew"
	label $f_ref.scl -text "Scale:"
        grid configure $f_ref.scl -row 3 -column 3 -sticky "snew"
        entry $f_ref.sclt -textvariable ::NSLD::scl -state normal
        pack $f_ref.sclt -side left
        grid configure $f_ref.sclt -row 3 -column 4 -sticky "snew"
	label $f_ref.sldtit -text "SLD (x10-6 A-2):"
        grid configure $f_ref.sldtit -row 7 -column 1 -sticky "snew"
	label $f_ref.thicktit -text "Thickness (A):"
        grid configure $f_ref.thicktit -row 7 -column 2 -sticky "snew"	
	label $f_ref.solvftit -text "Solvent Volume Fraction:"
        grid configure $f_ref.solvftit -row 7 -column 3 -sticky "snew"
	label $f_ref.roughtit -text "Roughness (A):"
        grid configure $f_ref.roughtit -row 7 -column 4 -sticky "snew"

	label $f_ref.lay0 -text "Layer 0:  "
        grid configure $f_ref.lay0 -row 8 -column 0 -sticky "snew"
	label $f_ref.lay1 -text "Layer 1:  "
        grid configure $f_ref.lay1 -row 9 -column 0 -sticky "snew"
	label $f_ref.lay2 -text "Layer 2:  "                                 ;# A: Extra backing layer between surface and substrate i.e.: more solvent
        grid configure $f_ref.lay2 -row 10 -column 0 -sticky "snew"           ;# A: Extra backing layer between surface and substrate i.e.: more solvent
	label $f_ref.laylast -text "Layer n+1:"                              ;# A: Extra backing layer between surface and substrate i.e.: more solvent
        grid configure $f_ref.laylast -row 11 -column 0 -sticky "snew"
	entry $f_ref.l0_sld -textvariable ::NSLD::SLD0 -state normal
        pack $f_ref.l0_sld -side left
        grid configure $f_ref.l0_sld -row 8 -column 1 -sticky "snew"
	entry $f_ref.l1_sld -textvariable ::NSLD::SLD1 -state normal
        pack $f_ref.l1_sld -side left
        grid configure $f_ref.l1_sld -row 9 -column 1 -sticky "snew"
	entry $f_ref.l1_thick -textvariable ::NSLD::thick1 -state normal
        pack $f_ref.l1_thick -side left
        grid configure $f_ref.l1_thick -row 9 -column 2 -sticky "snew"
	entry $f_ref.l1_solvf -textvariable ::NSLD::solvf1 -state normal
        pack $f_ref.l1_solvf -side left
        grid configure $f_ref.l1_solvf -row 9 -column 3 -sticky "snew"
	entry $f_ref.l1_rough -textvariable ::NSLD::rough1 -state normal
        pack $f_ref.l1_rough -side left
        grid configure $f_ref.l1_rough -row 9 -column 4 -sticky "snew"

        ############## Extra backing layer between surface and substrate - START ##############
	entry $f_ref.l2_sld -textvariable ::NSLD::SLD2 -state normal
        pack $f_ref.l2_sld -side left
        grid configure $f_ref.l2_sld -row 10 -column 1 -sticky "snew"
        entry $f_ref.l2_thick -textvariable ::NSLD::thick2 -state normal
        pack $f_ref.l2_thick -side left
        grid configure $f_ref.l2_thick -row 10 -column 2 -sticky "snew"
        entry $f_ref.l2_solvf -textvariable ::NSLD::solvf2 -state normal
        pack $f_ref.l2_solvf -side left
        grid configure $f_ref.l2_solvf -row 10 -column 3 -sticky "snew"
        entry $f_ref.l2_rough -textvariable ::NSLD::rough2 -state normal
        pack $f_ref.l2_rough -side left
        grid configure $f_ref.l2_rough -row 10 -column 4 -sticky "snew"
	############## Extra backing layer between surface and substrate - END ##############

	entry $f_ref.llast_sld -textvariable ::NSLD::SLDlast -state normal
        pack $f_ref.llast_sld -side left
        grid configure $f_ref.llast_sld -row 11 -column 1 -sticky "snew"
	entry $f_ref.llast_rough -textvariable ::NSLD::roughlast -state normal
        pack $f_ref.llast_rough -side left
        grid configure $f_ref.llast_rough -row 11 -column 4 -sticky "snew"
	label $f_ref.simrought -text "Sim. Roughness:"
        grid configure $f_ref.simrought -row 4 -column 3 -sticky "snew"	
	entry $f_ref.simrough -textvariable ::NSLD::rough -state normal
        pack $f_ref.simrough -side left
        grid configure $f_ref.simrough -row 4 -column 4 -sticky "snew"


#	grid rowconfigure $f_ref -weight 0 -minsize 10

}

proc ::NSLD::nsldrun {} {
	variable binsize
	variable mol
	variable amvsel
#	variable selstr
	variable grpname
	variable numsel	
	variable centresel
	variable firstframe
	variable lastframe
	variable stepsize
        variable calcnsld
        variable calcvolfr
	variable calcvolfr_perbin
        variable finegrain
        variable coarsegrain
	variable deutperc
	variable simSLDlist 
	variable thick
	variable deutsel


#Define output files
set output [open SLD.dat w]
set log [open nsld.log w]
set binsizecontrol [open binsize.dat w]
set boxdata [open box.dat w]
set vol_profile [open vol_profile.dat w]
set nsldTOTAL [open nsld_total.dat w]

#Reverse deuteration for the case when the run is interrupted (see proc defined at the begining).
d2w
d2wsel

#Deuterate user defined percentage of water molecules using proc w2d (see definition of procs at the begining).
w2d $deutperc

#Define list of elements
set elements {H D C N O Na Mg P S Cl K Ca Mn Fe Co Ni Cu Zn Br Mo Ru Rh Pd Li F Al Rb Cd I Cs Ba}

#Define b-values database
#NOTE:b values corresond to the average over isotopes (except for Cd which has complex number contributions and consequently the chosen value 
#is the one for the most abundant isotope (28.72%) <-- check before using!!!).
#set b_list {{H -3.7390} {D 6.671} {C 6.6460} {N 9.36} {O 5.803} {Na 3.63} {Mg 5.375} {P 5.13}\
	 {S 2.847} {Cl 9.5770} {K 3.67} {Ca 4.70} {Mn -3.73} {Fe 9.45} {Co 2.49} {Ni 10.3}\
	 {Cu 7.718} {Zn 5.680} {Br 6.795} {Mo 6.715} {Ru 7.03} {Rh 5.88} {Pd 5.91} {Li -1.90}\
	 {F 5.654} {Al 3.449} {Rb 7.09} {Cd 7.5} {I 5.28} {Cs 5.42} {Ba 5.07} {NC -6.017}\
	 {NH -3.521} {PH 28.342} {PHE 28.342} {GL 1.243} {EST1 17.42} {EST2 10.774} {CMD2 5.814}\
	 {CM -2.496} {CT -6.235} {CT2 -5.403} {W -5.025} {WD 57.435}}
set b_list {{H -3.7390} {D 6.671} {C 6.6460} {N 9.36} {O 5.803} {Na 3.63} {Mg 5.375} {P 5.13}\
         {S 2.847} {Cl 9.5770} {K 3.67} {Ca 4.70} {Mn -3.73} {Fe 9.45} {Co 2.49} {Ni 10.3}\
         {Cu 7.718} {Zn 5.680} {Br 6.795} {Mo 6.715} {Ru 7.03} {Rh 5.88} {Pd 5.91} {Li -1.90}\
         {F 5.654} {Al 3.449} {Rb 7.09} {Cd 7.5} {I 5.28} {Cs 5.42} {Ba 5.07} {NC -6.017}\
         {NH -3.521} {PH 28.342} {PHE 28.342} {GL 1.243} {EST1 17.42} {EST2 17.42} {CMD2 5.814}\
         {CM -2.496} {CT -6.235} {CT2 -5.403} {W -5.025} {WD 57.435}}

#Define atom types per element (FG)
#CHARMM | The definition of b for each atomname is done by getting the atomtype and linking the last with the corresponding element.
#	  This implies that the information of the atomtypes need to be loaded into vmd (i.e.: psf file) 
# In the case of deuterium, which is not found in the CHARMM forcefield, the atomtypes DEUT (water molecule) and DEU (non water molecule) are
# used for the purpose of this code.

set ats(H) {H HA HA1 HA2 HA3 HAL1 HAL2 HAL3 HB HB1 HB2 HBL HC\
	 HCA1 HCA1C2 HCA2 HCA2C2 HCA3 HCA3M HCL HCP1 HCP1M\
	 HCR1 HE1 HE2 HEL1 HEL2 HGA1 HGA2 HGA3 HGA4 HGA5\
	 HGA6 HGA7 HGAAM0 HGAAM1 HGAAM2 HGP1 HGP2 HGP3\
	 HGP4 HGP5 HGPAM1 HGPAM2 HGPAM3 HGR51 HGR52 HGR53\
	 HGR61 HGR62 HGR63 HGR71 HL HN1 HN2 HN3 HN4 HN5 HN6\
	 HN7 HN8 HN9 HOL HP HR1 HR2 HR3 HS HT HX}
set ats(D) {DEUT DEU}
set ats(C) {C CA CAI CC CC2O1 CC2O2 CC2O3 CC2O4 CC301 CC3051\
	 CC3062 CC311 CC311C CC311D CC312 CC312C CC312D CC3151\
	 CC3152 CC3153 CC3161 CC3162 CC3163 CC321 CC321C CC321D\
	 CC322 CC322C CC3251 CC3261 CC3263 CC331 CCL CD CE1 CE2\
	 CEL1 CEL2 CG1N1 CG1T1 CG251O CG252O CG25C1 CG25C2 CG2D1\
	 CG2D1O CG2D2 CG2D2O CG2DC1 CG2DC2 CG2DC3 CG2N1 CG2N2\
	 CG2O1 CG2O2 CG2O3 CG2O4 CG2O5 CG2O6 CG2O7 CG2R51 CG2R52\
	 CG2R53 CG2R61 CG2R62 CG2R63 CG2R64 CG2R66 CG2R67 CG2R71\
	 CG2RC0 CG2RC7 CG301 CG302 CG311 CG312 CG314 CG321 CG322\
	 CG323 CG324 CG331 CG334 CG3AM0 CG3AM1 CG3AM2 CG3C31 CG3C41\
	 CG3C50 CG3C51 CG3C52 CG3C53 CG3C54 CG3RC1 CL CN1 CN1T CN2\
	 CN3 CN3T CN4 CN5 CN5G CN7 CN7B CN8 CN8B CN9 CP1 CP2 CP3\
	 CPH1 CPH2 CPT CRL1 CRL2 CS CT1 CT CT2 CT2A CT3 CTL1 CTL2\
	 CTL3 CTL5 CY}
set ats(N) {N NC2 NC2D1 NG1T1 NG2D1 NG2O1 NG2P1 NG2R43 NG2R50 NG2R51\
	 NG2R52 NG2R53 NG2R60 NG2R61 NG2R62 NG2RC0 NG2S0 NG2S1 NG2S2\
	 NG2S3 NG301 NG311 NG321 NG331 NG3C51 NG3N1 NG3P0 NG3P1 NG3P2\
	 NG3P3 NH1 NH2 NH3 NH3L NN1 NN2 NN2B NN2G NN2U NN3 NN3A NN3G\
	 NN4 NN6 NP NR1 NR2 NR3 NTL NY}
set ats(O) {O OB OC OH1 O2L OBL OC2D1 OC2D2 OC2D3 OC2D4 OC2DP OC301\
	 OC302 OC303 OC30P OC311 OC311M OC312 OC3C51 OC3C5M OC3C61\
	 OCL OG2D1 OG2D2 OG2D3 OG2D4 OG2D5 OG2N1 OG2P1 OG2R50 OG301\
	 OG302 OG303 OG304 OG311 OG312 OG3C31 OG3C51 OG3C61 OG3R60\
	 OHL ON1 ON1C ON2 ON3 ON4 ON5 ON6 ON6B OS OSL OSLP OT OX}
set ats(P) {PG0 PG1 PG2 P2 P PC PL}
set ats(Na) {SOD}
set ats(Cl) {CLA CLGA1 CLGA3 CLGR1}
set ats(K) {POT}
set ats(Ca) {CAL}
set ats(Mg) {MG}
set ats(S) {S SC SG2D1 SG2R50 SG301 SG302 SG311 SG3O1 SG3O2 SG3O3 SL SM SS}
set ats(Mn) {}
set ats(Fe) {}
set ats(Co) {}
set ats(Ni) {}
set ats(Cu) {}
set ats(Zn) {ZN}
set ats(Br) {BRGA1 BRGA2 BRGA3 BRGR1}
set ats(Mo) {}
set ats(Ru) {}
set ats(Rh) {}
set ats(Pd) {}
set ats(Li) {LIT}
set ats(F) {FGA1 FGA2 FGA3 FGP1 FGR1}
set ats(Al) {ALG1}
set ats(Rb) {RUB}
set ats(Cd) {CAD}
set ats(I) {IGR1}
set ats(Cs) {CES}
set ats(Ba) {BAR}

#Define list of bead types (CG)
set SDKcgtype {W WD CT CM CT2 CQT CQM CBT CMD2 OA EOT EO MOH EOH EOC CTE EST EST1 EST2 SOD CLA NC4 SO4 PH NC GL PHE NH OB}

#Define bead names per bead type (CG)
#SDK_CG_FF

#DOPC
#set typ(DOPC) {NC PH GL EST1 EST2 CM CMD2 CT2}
#set ats(DOPC,NC) {NC}
#set ats(DOPC,PH) {PH}
#set ats(DOPC,GL) {GL}
#set ats(DOPC,EST1) {EST1}
#set ats(DOPC,EST2) {EST2}
#set ats(DOPC,CM) {C11 C12 C14 C15 C21 C22 C24 C25}
#set ats(DOPC,CMD2) {C13 C23}
#set ats(DOPC,CT) {}
#set ats(DOPC,CT2) {C16 C26}

#DPPC
#set typ(DPPC) {NC PH GL EST1 EST2 CM CT2}
#set ats(DPPC,NC) {NC}
#set ats(DPPC,PH) {PH}
#set ats(DPPC,GL) {GL}
#set ats(DPPC,EST1) {EST1}
#set ats(DPPC,EST2) {EST2}
#set ats(DPPC,CM) {C11 C12 C13 C14 C21 C22 C23 C24}
#set ats(DPPC,CT2) {C15 C25}

#DMPC
#set typ(DMPC) {NC PH GL EST1 EST2 CM CT}
#set ats(DMPC,NC) {NC}
#set ats(DMPC,PH) {PH}
#set ats(DMPC,GL) {GL}
#set ats(DMPC,EST1) {EST1}
#set ats(DMPC,EST2) {EST2}
#set ats(DMPC,CM) {C11 C12 C13 C21 C22 C23}
#set ats(DMPC,CT) {C14 C24}

#WATER
#set typ(WAT) {W WD}
#set ats(WAT,W) {W}
#set ats(WAT,WD) {WD}


#Define CG beads radii
# WD=Deuterated Water. We assume the same radii as the SDK-CG water
set CGrad(W) 2.45314
set CGrad(WD) 2.45314
set CGrad(CT) 2.57324
set CGrad(CM) 2.52891
set CGrad(CT2) 2.36896
set CGrad(CQT) 2.81738
set CGrad(CQM) 2.79493
set CGrad(CBT) 2.74105
set CGrad(CMD2) 2.24773
set CGrad(OA) 2.08385
set CGrad(EOT) 2.38523
set CGrad(EO) 2.38523
set CGrad(MOH) 2.06533
set CGrad(EOH) 2.29151
set CGrad(EOC) 2.38523
set CGrad(CTE) 2.57324
set CGrad(EST) 2.41329
set CGrad(EST1) 2.41329
set CGrad(EST2) 2.41329
set CGrad(SOD) 2.45314
set CGrad(CLA) 2.45314
set CGrad(NC4) 3.33932
set CGrad(SO4) 2.42508
set CGrad(PH) 3.03065
set CGrad(NC) 3.22708
set CGrad(GL) 2.52891
set CGrad(PHE) 2.58166
set CGrad(NH) 2.58166
set CGrad(OB) 2.29151

#if { $coarsegrain } {
#foreach CGmol {DOPC DPPC DMPC WAT} {
#	foreach CGtype $typ($CGmol) {
#		set sel [atomselect top "name $ats($CGmol,$CGtype)"]
#		$sel set radius $CGrad($CGtype)
#		$sel delete
#	}
#}
#}

if { $coarsegrain } {
	foreach CGtype $SDKcgtype {
                set sel [atomselect top "type $CGtype"]
                $sel set radius $CGrad($CGtype)
                $sel delete
	}
}

#################--- Definition of vdW radii ---####################
# To avoid default definition of radii done by VMD, which can make
# mistakes in the case of elements named with two letters (i.e.: Cl)
# The vdW radii was taken from A. Bondi (J.Phys.Chem.,68,441-452,1964)
# except for the value for H which is taken from R.S.Rowland,R.Taylor,
# (J.Phys.Chem.,100,7384-7391, 1996).
# In the case of ions (SOD, POT and CLA) the radii is based on the 
# CHARMM27 Rmin/2 parameter. In CHARMM36 there is a new Rmin/2 for Na
# (SOD      0.0       -0.0469    1.41075) but for consistency with the
# VMD definition, the value from CHARMM27 was kept.
# The value for deuterium was chosen equal to Hydrogen due to lack of
# references. Need to be further checked! 

set radii(H)     1.09
set radii(C)     1.7
set radii(Cl)    1.75
set radii(N)     1.55
set radii(O)     1.52
set radii(P)     1.8
set radii(S)     1.8
set radii(Br)    1.85
set radii(F)     1.47
set radii(Na_i)  1.36375
set radii(K_i)   1.76375
set radii(Cl_i)  2.27
set radii(Mg_i)  1.18500
set radii(D)     1.09

set r_ats(H) {H HA HA1 HA2 HA3 HAL1 HAL2 HAL3 HB1 HB2 HBL HC HCA1 HCA1C2 HCA2 HCA2C2 HCA3\
         HCA3M HCL HCP1 HCP1M HCR1 HE1 HE2 HEL1 HEL2 HGA1 HGA2 HGA3 HGA4 HGA5 HGA6 HGA7\
         HGAAM0 HGAAM1 HGAAM2 HGP1 HGP2 HGP3 HGP4 HGP5 HGPAM1 HGPAM2 HGPAM3 HGR51 HGR52\
         HGR53 HGR61 HGR62 HGR63 HGR71 HL HN1 HN2 HN3 HN4 HN5 HN6 HN7 HN8 HN9 HOL HP HR1\
         HR2 HR3 HS HT HX}

set r_ats(D) {DEUT DEU}

set r_ats(C) {C CA CAI CC CC2O1 CC2O2 CC2O3 CC2O4 CC301 CC3051 CC3062 CC311 CC311C CC311D\
         CC312 CC312C CC312D CC3151 CC3152 CC3153 CC3161 CC3162 CC3163 CC321 CC321C CC321D\
         CC322 CC322C CC3251 CC3261 CC3263 CC331 CCL CD CE1 CE2 CEL1 CEL2 CG1N1 CG1T1 CG251O\
         CG252O CG25C1 CG25C2 CG2D1 CG2D1O CG2D2 CG2D2O CG2DC1 CG2DC2 CG2DC3 CG2N1 CG2N2 CG2O1\
         CG2O2 CG2O3 CG2O4 CG2O5 CG2O6 CG2O7 CG2R51 CG2R52 CG2R53 CG2R61 CG2R62 CG2R63 CG2R64\
         CG2R66 CG2R67 CG2R71 CG2RC0 CG2RC7 CG301 CG302 CG311 CG312 CG314 CG321 CG322 CG323\
         CG324 CG331 CG334 CG3AM0 CG3AM1 CG3AM2 CG3C31 CG3C41 CG3C50 CG3C51 CG3C52 CG3C53\
         CG3C54 CG3RC1 CL CN1 CN1T CN2 CN3 CN3T CN4 CN5 CN5G CN7 CN7B CN8 CN8B CN9 CP1 CP2\
         CP3 CPH1 CPH2 CPT CRL1 CRL2 CS CT1 CT CT2 CT2A CT3 CTL1 CTL2 CTL3 CTL5 CY}

set r_ats(N) {N NC2 NC2D1 NG1T1 NG2D1 NG2O1 NG2P1 NG2R43 NG2R50 NG2R51 NG2R52 NG2R53 NG2R60\
         NG2R61 NG2R62 NG2RC0 NG2S0 NG2S1 NG2S2 NG2S3 NG301 NG311 NG321 NG331 NG3C51 NG3N1\
         NG3P0 NG3P1 NG3P2 NG3P3 NH1 NH2 NH3 NH3L NN1 NN2 NN2B NN2G NN2U NN3 NN3A NN3G NN4\
         NN6 NP NR1 NR2 NR3 NTL NY}

set r_ats(O) {O OB OC OH1 O2L OBL OC2D1 OC2D2 OC2D3 OC2D4 OC2DP OC301 OC302 OC303 OC30P OC311\
         OC311M OC312 OC3C51 OC3C5M OC3C61 OCL OG2D1 OG2D2 OG2D3 OG2D4 OG2D5 OG2N1 OG2P1 OG2R50\
         OG301 OG302 OG303 OG304 OG311 OG312 OG3C31 OG3C51 OG3C61 OG3R60 OHL ON1 ON1C ON2 ON3\
         ON4 ON5 ON6 ON6B OS OSL OSLP OT OX}

set r_ats(P) {PG0 PG1 PG2 P2 P PC PL}
set r_ats(Na_i) {SOD}
set r_ats(Cl_i) {CLA}
set r_ats(Cl) {CLGA1 CLGA3 CLGR1}
set r_ats(K_i) {POT}
set r_ats(Mg_i) {MG}
set r_ats(S) {S SC SG2D1 SG2R50 SG301 SG302 SG311 SG3O1 SG3O2 SG3O3 SL SM SS}
set r_ats(Br) {BRGA1 BRGA2 BRGA3 BRGR1}
set r_ats(F) {FGA1 FGA2 FGA3 FGP1 FGR1}

set r_atoms {H C Cl N O P S Br F Na_i K_i Cl_i Mg_i}

if { $finegrain } {
foreach r_atom $r_atoms {
        foreach TYPE $r_ats($r_atom) {
                set sel [atomselect top "type $TYPE"]
                $sel set radius $radii($r_atom)
        }
}
}

#Assign b-value per element
puts $log "b-values:"
foreach x $b_list {
        set b([lindex $x 0]) [lindex $x 1]
        puts $log "[lindex $x 0] $b([lindex $x 0])"
}

#Loop over the groups defined as input 
set grplist []
puts "SYSTEM DECOMPOSITION:"
for {set i 0} {$i<$numsel} {incr i} {		
	if {$mol($i) != ""} {
		puts "COMPONENT-$i: $mol($i)"
		#Group specific deuteration
		if {$deutsel($i) != ""} {
			w2dsel $deutsel($i) $mol($i) 
		}
		set grplist [lappend grplist $i]
		puts $log "Generating groups from molecule: $mol($i) ..."
		#Obtain the number of atoms per molecule type
#		set molsel [atomselect top "$mol($i)"]
#		set fullsel [atomselect top all]
#		set molseltype($i) [lindex $mol($i) 0]
#		set molselsel($i) [lindex $mol($i) 1]
		if {$grpname($i) == ""} {
			set grpname($i) sel_${i}
		}
#		set nat($i) [[atomselect top "$mol($i) and residue [lindex [$fullsel get residue] [lsearch [$fullsel get $molseltype($i)] $molselsel($i)]]"] num]
#		set nat($i) [[atomselect top "$mol($i)"]]
#		puts $nat($i)
#		set mol_nr($i) [expr [$molsel num]/$nat($grpname($i))]
#		puts $mol_nr($i)
#		$fullsel delete
#		$molsel delete
		#Obtain the atoms of each group
#		if {$selstr($i) != ""} {
#			set selstr_sel [atomselect top "$mol($i) and name $selstr($i)"]
#		} else {
#			set selstr_sel [atomselect top "$mol($i)"]
#		}

		set selstr_sel [atomselect top "$mol($i)"]
		set gratoms($i) [lsort -unique [$selstr_sel get name]]
#		set gr_atnum($i) [expr [$selstr_sel num]/$mol_nr($i)]
#		puts $gr_atnum($i)
		puts $log "Group: $grpname($i):"
		puts $log "$gratoms($i)"
#		set sys_at [lappend sys_at $gratoms($i)]
		#Obtain the elements present in each group
		set eltemplist []
			#print atomtypes control data
			puts $log "Atomtypes Control:"
			foreach at $gratoms($i) {
				if { $finegrain } {
					puts $log "The analysis selected is for atomistic data"
					set atype [lindex [lsort -unique [[atomselect top "name $at and ($mol($i))"] get type]] 0]
					foreach el $elements {
						if {[lsearch $ats($el) $atype]!= -1} {
                                                        set eltemplist [lappend eltemplist $el]
                                                        break
                                                }
					}
					puts $log "$at $atype $el"
				} elseif { $coarsegrain } {
					puts $log "The analysis selected is for coarse-grained data"
					set btype [lindex [lsort -unique [[atomselect top "name $at and ($mol($i))"] get type]] 0]	
					set eltemplist [lappend eltemplist $btype]
				}
			}
		set grelements($i) [lsort -unique $eltemplist]
		puts $log "Elements (FG) or beadtypes (CG) in group $grpname($i): $grelements($i)"
		#Divide groups in subgroups according to the element
		set grpsubgrps($i) []
		foreach k $grelements($i) {
			set tempsubgr($k) []
			foreach at $gratoms($i) {
				if { $finegrain } {           
					set atype [lindex [lsort -unique [[atomselect top "name $at and $mol($i)"] get type]] 0]
                                        foreach el $elements {
                                                if {[lsearch $ats($el) $atype]!= -1} {
                                                        break
                                                }
                                        }
				} elseif { $coarsegrain } {    
					set btype [lindex [lsort -unique [[atomselect top "name $at and ($mol($i))"] get type]] 0]
					set el $btype
				}                                
				if {$el == $k} {
					set tempsubgr($k) [lappend tempsubgr($k) $at]
				}
			}		
			set grpsubgrps($i) [lappend grpsubgrps($i) $tempsubgr($k)]
		}	
		puts $log "Group $grpname($i) subgroups: $grpsubgrps($i)"
		$selstr_sel delete
	} else {
		puts "COMPONENT-$i: ---"
	}
}

#Get number of frames
set num_simfr [molinfo top get numframes]

#Obtain box dimensions
set xdim [molinfo top get a]
set ydim [molinfo top get b]
set zdim [molinfo top get c]
puts $log "Initial box dimensions:"
puts $log "x:$xdim"
puts $log "y:$ydim"
puts $log "z:$zdim"

#Define number of bins in z direction
set num_bins [expr round(floor($zdim/$binsize))]
puts $log "Number of bins: $num_bins"

#Define data structure
#Loop over bins
set minbin [expr round(floor(-($num_bins)/2))]
set maxbin [expr $num_bins+round(ceil(($num_bins)/2))]
for {set j $minbin} {$j<=$maxbin} {incr j} {
	set gr_col []
	#Loop over user defined groups
	for {set i 0} {$i<$numsel} {incr i} {
		if {$mol($i) != ""} {
			lappend gr_col 0	
		}
	}
	set row($j) $gr_col   	
	set row_vol($j) $gr_col
}

#Set lastframe to analyze
if {$lastframe == ""} {
	set lastframe [expr $num_simfr-1]
}
#Set the effective number of frames used for the NSLD calculation
set R_numfr_ss [expr fmod(($lastframe - $firstframe + 1),double($stepsize))]
if {$R_numfr_ss!=[expr double(0)]} {
	set num_fr [expr int(($lastframe - $firstframe + 1)/double($stepsize))+1]
} else {
	set num_fr [expr int(($lastframe - $firstframe + 1)/double($stepsize))]
}
puts $log "Number of frames to use for NSLD calculation: $num_fr (taken every $stepsize simulation frames)"
#Define list to allocate dynamic bin list elements
set binlist []

puts $log "STARTING NSLD CALCULATION ..."
puts "STARTING CALCULATION ..."

#Define absolute volumes output files
if { $calcvolfr || $calcvolfr_perbin } {
	foreach amv_g $grplist {
		if {$amvsel($amv_g) != ""} {
			set volout_amv($amv_g) [open vol_$grpname($amv_g).dat w]
			puts $volout_amv($amv_g) "@frame n_residues total_vol avg_residue_vol"
		}
	}
}


set avg_binsize 0
#Loop over the simulation frames
for {set i $firstframe} {$i<=$lastframe} {incr i $stepsize} {
	puts "Progress: [expr 100*$i/($num_fr*$stepsize)]% (frame $i)"	
	puts $log "Analizing frame: $i ..."
	#Obtain box dimensions for each frame
	set xdim [molinfo top get a frame $i]
	set ydim [molinfo top get b frame $i]
	set zdim [molinfo top get c frame $i]
	#Get the minimum and maximum atomic coordinates in the z axis
	set selbox [atomselect top "all" frame $i]
	set minz [lindex [measure minmax $selbox] 0 2]
	set maxz [lindex [measure minmax $selbox] 1 2]
	set zdim_minmax [expr $maxz-$minz]
	$selbox delete
	#Calculate the minimum and maximum positions of the box in the z axis
	set boxshift [expr ($zdim_minmax-$zdim)/2]
	set boxzmin [expr $minz+$boxshift] 
	set boxzmax [expr $maxz-$boxshift]
	#Recalculate binsize considering z-dim fluctuations
	puts $boxdata "$i $xdim $ydim $zdim"
	set binsize [expr ($boxzmax-$boxzmin)/$num_bins]
	puts $binsizecontrol "$i $binsize"
	#Calculate average binsize
	set avg_binsize [expr $avg_binsize + ($binsize/$num_fr)]
	#Obtain geometric centre (z axis component) of user defined selection (i.e.:bilayer) to centre the plot
	set censel [atomselect top $centresel frame $i]
	set centre [lindex [measure center $censel] 2]
	$censel delete
	#Obtain the position of the boundary for shifting the box due to bilayer movement during the simulation
	set centreshift [expr ($centre-$boxzmin)-(($boxzmax-$boxzmin)/2)]
	if {$centreshift>=0} {
		set boundary [expr $centre-(($boxzmax-$boxzmin)/2)]
	} else {
		set boundary [expr $centre+(($boxzmax-$boxzmin)/2)]
	}
	#Loop over bins to get the one which contain the boundary
	set j 0
        set m [expr $boxzmin+$j*$binsize]
        set M [expr $boxzmin+$binsize+$j*$binsize]
	while {$boundary<$m || $boundary>=$M} {
		incr j
                set m [expr $boxzmin+$j*$binsize]
                set M [expr $boxzmin+$binsize+$j*$binsize]
	}	
	if {$boundary<$centre} {
		set lowboundbin $j
		set highboundbin [expr $num_bins -1]
	} else {
		set lowboundbin 0
		set highboundbin $j
	}
	#Compute voronoi 3d volumes
	if { $calcvolfr || $calcvolfr_perbin } {
#		puts "VOLUME FRACTION CALCULATION IS ON"
		set selall [atomselect top "all" frame $i]
		set SysInxList [$selall get index]
		#Reset volumes per particle to 0 A^3. (See "Comments.txt")
		foreach sinx $SysInxList {
			set vol($sinx) 0.0
		}
		if { $calcvolfr } {
		set voroin [open voroin.$i w]  ;# XXX
		set seldata [$selall get {residue index x y z radius}] ;#XXX
		}
                set mM [measure minmax $selall]
                set xm [lindex $mM 0 0]
                set ym [lindex $mM 0 1]
                set xM [lindex $mM 1 0]  
                set yM [lindex $mM 1 1]

		if { $calcvolfr } {		
		set zm [lindex $mM 0 2] ;# XXX
                set zM [lindex $mM 1 2] ;# XXX
                foreach data $seldata {  ;# XXX
                        puts $voroin "[lindex $data 1] [lrange $data 2 5]" ;# XXX
                } ;# XXX
                close $voroin ;# XXX
		exec voro++ -r -c "%i %v" -p $xm $xM $ym $yM $zm $zM voroin.$i ;# XXX

		set volout [open "voroin.$i.vol" r] ;# XXX
		set VolFileSize [file size "voroin.$i.vol"] ;# XXX
                set voldata [split [read $volout $VolFileSize] \n] ;# XXX
                close $volout ;# XXX
                exec rm voroin.$i.vol voroin.$i ;# XXX
#                set voldatalist_temp [split $voldata "\n"]
#                set voldatalist [lsearch -all -inline -not $voldata {}]
		foreach inx $voldata {  ;# XXX
			set vol([lindex $inx 0]) [lindex $inx 1] ;# XXX
#			puts $vol([lindex $inx 0])
		} ;# XXX
		}

		$selall delete
	}
	#Loop over bins
	for {set j 0} {$j<=$num_bins-1} {incr j} {

		#Define z minimum (m) and maximum (M)
		set m [expr $boxzmin+$j*$binsize]
		set M [expr $boxzmin+$binsize+$j*$binsize]
		#Ask if this is the boundbin
		if {$boundary<$centre} {
			if {$j<$lowboundbin} {
				set BIN [expr $j+$num_bins-$lowboundbin]
			} else {
				set BIN [expr $j-$lowboundbin]
			}	
        	} else {
                	if {$j>$highboundbin} {
				set BIN [expr $j-$num_bins+($num_bins-$highboundbin-1)]
			} else {
				set BIN [expr $j+($num_bins-$highboundbin-1)]
			}
        	}
		lappend binlist $BIN

		if {$calcvolfr_perbin} {
 	               #######################################################
			#Added patch of code for per bin voro++
			#See commented lines marked "XXX" from previous version
			set voroin [open voroin.$i w]
			set at_sel_vol_all [atomselect top "(z>=$m and z<$M)" frame $i]
			set seldata [$at_sel_vol_all get {residue index x y z radius}]
			foreach data $seldata {
                        	puts $voroin "[lindex $data 1] [lrange $data 2 5]"
                        }
                        $at_sel_vol_all delete
                        close $voroin
                        exec voro++ -r -c "%i %v" -p $xm $xM $ym $yM $m $M voroin.$i
                        set volout [open "voroin.$i.vol" r]
                        set VolFileSize [file size "voroin.$i.vol"]
                        set voldata [split [read $volout $VolFileSize] \n]
                        close $volout
                        exec rm voroin.$i.vol voroin.$i
                        foreach inx $voldata {
#                        	puts [lindex $inx 1]
                                set vol([lindex $inx 0]) [lindex $inx 1]
                        }
                        #######################################################
		}


		#Loop over groups defined by the user
		set index 0		
		foreach k $grplist {
			#Compute volume for each group defined by the user
			if { $calcvolfr || $calcvolfr_perbin } {
				set at_sel_vol [atomselect top "($mol($k)) and (z>=$m and z<$M)" frame $i]
				set v_sel_inx [$at_sel_vol get index] 
				$at_sel_vol delete

#				if {$calcvolfr_perbin} {
#				#######################################################
#				#Added patch of code for per bin voro++
#				#See commented lines marked "XXX" from previous version
#				set voroin [open voroin.$i w]
#				set at_sel_vol_all [atomselect top "(z>=$m and z<$M)" frame $i]
#				set seldata [$at_sel_vol_all get {residue index x y z radius}]
#				foreach data $seldata {
#		                        puts $voroin "[lindex $data 1] [lrange $data 2 5]"
#                		}
#				$at_sel_vol_all delete
#                		close $voroin
#				exec voro++ -r -c "%i %v" -p $xm $xM $ym $yM $m $M voroin.$i
#				set volout [open "voroin.$i.vol" r]
#				set VolFileSize [file size "voroin.$i.vol"]
#				set voldata [split [read $volout $VolFileSize] \n]
#				close $volout
#				exec rm voroin.$i.vol voroin.$i
#				foreach inx $voldata {
#					puts [lindex $inx 1]
#		                        set vol([lindex $inx 0]) [lindex $inx 1]
#                		}
#				#######################################################
#				}

				set vol_grp 0
				foreach v_inx $v_sel_inx {
#					set vol_inx [lindex [lsearch -all -inline -index 0 $voldatalist $v_inx] 0 1]
#					set vol_grp [expr $vol_inx + $vol_grp]
					set vol_grp [expr $vol($v_inx) + $vol_grp]
				}
				set vol_frac_grp [expr ($vol_grp)/($xdim*$ydim*$binsize)]
				set row_vol($BIN) [lreplace $row_vol($BIN) $index $index [expr ($vol_frac_grp)/($num_fr)+[lindex $row_vol($BIN) $index]]]
			}
			#Compute Neutron Scattering Length
			if { $calcnsld } {
#				puts "NSLD CALCULATION IS ON"
				set at_len 0
				set tot_at_gr 0
				#Loop over the subgroups
				for {set subgr 0} {$subgr<=[llength $grelements($k)]-1} {incr subgr} {
					set selsubgr [lindex $grpsubgrps($k) $subgr]
					set at_sel [atomselect top "(name $selsubgr) and ($mol($k)) and (z>=$m and z<$M)" frame $i]
					#Compute subgroup NSL contribution and sum for all subgroups
					#The 10 factor tranforms the units from 10-5A-2 to 10-6A-2	
	#				set at_len [expr 10*[$at_sel num]*$b([lindex $grelements($k) $subgr]) + $at_len]
					set atnum [$at_sel num]
					set tot_at_gr [expr $atnum + $tot_at_gr]
					set at_len [expr 10*$atnum*$b([lindex $grelements($k) $subgr]) + $at_len]
					$at_sel delete
				}
				#Divide the NSL by the box slice volume
				set l [expr ($at_len)/($xdim*$ydim*$binsize)]
				#Update output
				set row($BIN) [lreplace $row($BIN) $index $index [expr ($l/($num_fr))+[lindex $row($BIN) $index]]]
			}
			set index [expr $index+1]
		}
	}

	#Obtain Absolute Molecular Volumes on each frame
	if { $calcvolfr || $calcvolfr_perbin } {
		foreach amv_g $grplist {
#			set avg_amv($amv_g) 0
			if {$amvsel($amv_g) != ""} {
				set amv_sel [atomselect top "$mol($amv_g) and ($amvsel($amv_g))" frame $i]
				set amv_residues [lsort -unique [$amv_sel get residue]]	
				set amv_index [$amv_sel get index]
				set num_amv_residues [llength $amv_residues]
				set vol_amv($amv_g) 0
				foreach ix $amv_index {
					set vol_amv($amv_g) [expr $vol_amv($amv_g) + $vol($ix)]
				}
				puts $volout_amv($amv_g) "$i $num_amv_residues $vol_amv($amv_g) [expr $vol_amv($amv_g)/$num_amv_residues]"
#				set avg_amv($amv_g) [expr $avg_amv($amv_g) + $vol_amv($amv_g)/$num_fr]	
				$amv_sel delete
			}
		}
	}

}

#Define thick from avg_binsize to export to reflectivity procedure
set thick $avg_binsize

set col_bins []
set col_sum_sld []
set col_sum_vol []
set binlist [lsort -unique -integer $binlist]
foreach j $binlist {
	#Write output
	puts $output "$j $row($j)"
	puts $vol_profile "$j $row_vol($j)"
	#Define bins column for plotting
	set bin_z [expr $j*$binsize]
	set col_bins [lappend col_bins $bin_z]
	#Define groups columns for plotting
	set colnum 0
	set row_sum 0
	set row_vol_sum 0
	for {set i 0} {$i<$numsel} {incr i} {
		if {$mol($i) != ""} {
			set col($i) [lappend col($i) [lindex $row($j) $colnum]]		
			set row_sum [expr $row_sum + [lindex $row($j) $colnum]]	
			set col_vol($i) [lappend col_vol($i) [lindex $row_vol($j) $colnum]]	
			set row_vol_sum  [expr $row_vol_sum + [lindex $row_vol($j) $colnum]]
			set colnum [expr $colnum + 1]
		}
	}
	set col_sum_sld [lappend col_sum_sld $row_sum]
	set col_sum_vol [lappend col_sum_vol $row_vol_sum]
	#Write total NSLD to file
	puts $nsldTOTAL "$bin_z $row_sum"
}

#Define simSLDlist to export to reflectivity proc
set simSLDlist $col_sum_sld

puts $log "CALCULATION COMPLETED"
puts "CALCULATION COMPLETED :)"

#Close output files
close $output
close $log
close $binsizecontrol
close $boxdata
close $vol_profile
close $nsldTOTAL

if { $calcvolfr || $calcvolfr_perbin } {
	foreach amv_g $grplist {
		if {$amvsel($amv_g) != ""} {
			close $volout_amv($amv_g)
		}
	}
}


#Reverse deuteration using proc d2w (see definintion of procs at the begining)
d2w
d2wsel

#Plot NSLD using multiplot
package require multiplot
#set grplist_lessfirst [lrange $grplist 1 [llength $grplist]-1]
if { $calcnsld } {
	set plothandle [multiplot -x $col_bins -y $col(0) -title "Neutron Scattering Length Density Plot" -linewidth 2.0 -marker point -xlabel "box (A)" -ylabel "    NSLD\n(10^-6 A^-2)" -legend $grpname(0) -plot]
	set grplist_lessfirst [lrange $grplist 1 [llength $grplist]-1]
	foreach i $grplist_lessfirst {
		$plothandle add $col_bins $col($i) -linewidth 2.0 -marker point  -legend $grpname($i)  -plot 
	}
	$plothandle add $col_bins $col_sum_sld -linewidth 2.0 -marker point  -legend Total  -plot
}
##Plot Volume Fraction using multiplot
if { $calcvolfr || $calcvolfr_perbin } {
	set plothandle_vol [multiplot -x $col_bins -y $col_vol(0) -title "Volume Fraction" -linewidth 2.0 -marker point -xlabel "box (A)" -ylabel "    VOL\n   (A^3)" -legend $grpname(0) -plot]
	set grplist_lessfirst [lrange $grplist 1 [llength $grplist]-1]
	foreach i $grplist_lessfirst {
	        $plothandle_vol add $col_bins $col_vol($i) -linewidth 2.0 -marker point  -legend $grpname($i)  -plot
	}
	$plothandle_vol add $col_bins $col_sum_vol -linewidth 2.0 -marker point  -legend Total  -plot
}
}

# ------------------ Reflectivity Calculation -------------------
proc ::NSLD::ref {} {
	package require math::linearalgebra
	package require math::complexnumbers
	variable Qmin            ; #Moment transfer (Q) minimum value
        variable Qmax            ; #Moment transfer (Q) maximum value
        variable Qngrid          ; #Moment transfer (Q) number of grid points
        variable SLD0            ; #SLD of layer 0
        variable SLD1            ; #SLD of layer 1
        variable thick1          ; #Thickness of layer 1
        variable rough1          ; #Roughness of layer 1
	variable SLD2            ; #SLD of layer 2
        variable thick2          ; #Thickness of layer 2
        variable rough2          ; #Roughness of layer 2
        variable SLDlast         ; #SLD of layer n+1
        variable roughlast       ; #Roughness of layer n+1
        variable rough           ; #Roughness for all simulation layers
        variable bcgr            ; #background signal in experiment
        variable scl             ; #Scale factor
	variable simSLDlist      ; #List exported from nsldrun to ref proc
	variable sim_sld         ; #Flag for checkbutton controlling the simNSLD-based reflectivity
	variable man_sld         ; #Flag for checkbutton controlling the manual reflectivity 
	variable man_q           ; #Flag for checkbutton controlling the manual input of Q range
	variable exp_q           ; #Flag for checkbutton controlling the experimental input of Q range
	variable expfile         ; #path of the file containing the experimental reflectivity
        variable SLDmemory       ; #Flag for checkbutton controlling the usage of NLSD in memory (New MD-NSLD)
        variable SLDload         ; #Flag for checkbutton controlling the loading of NLSD save to disk (load MD-NSLD)
        variable savedNSLDfile   ; #path of the file containing the NSLD saved to disk (load MD-NSLD)
	variable thick           ; #Average binsize during the simulation ~ thicknes of simulation layers
	variable fronting        ; #Number of sim NSLD bins to discard from the left
	variable backing         ; #Number of sim NSLD bins to discard from the right
	
	proc mult_cplx_mat {A B} {

        	#Procedure to calculate 2x2 product of two complex numbers matrices, i.e.: A and B      

        	#Get matrices A and B elements
		set a00 [::math::linearalgebra::getelem $A 0 0]
        	set a01 [::math::linearalgebra::getelem $A 0 1]
        	set a10 [::math::linearalgebra::getelem $A 1 0]
        	set a11 [::math::linearalgebra::getelem $A 1 1]
        	set b00 [::math::linearalgebra::getelem $B 0 0]
        	set b01 [::math::linearalgebra::getelem $B 0 1]
        	set b10 [::math::linearalgebra::getelem $B 1 0]
        	set b11 [::math::linearalgebra::getelem $B 1 1]

		#Calculate product matrix (C) elements
        	set c00 [::math::complexnumbers::+ [::math::complexnumbers::* $a00 $b00] [::math::complexnumbers::* $a01 $b10]]
        	set c01 [::math::complexnumbers::+ [::math::complexnumbers::* $a00 $b01] [::math::complexnumbers::* $a01 $b11]]
        	set c10 [::math::complexnumbers::+ [::math::complexnumbers::* $a10 $b00] [::math::complexnumbers::* $a11 $b10]]
        	set c11 [::math::complexnumbers::+ [::math::complexnumbers::* $a10 $b01] [::math::complexnumbers::* $a11 $b11]]

        	#Build matrix C
        	set C [::math::linearalgebra::mkMatrix 2 2 0]
        	::math::linearalgebra::setelem C 0 0 $c00
        	::math::linearalgebra::setelem C 0 1 $c01
        	::math::linearalgebra::setelem C 1 0 $c10
        	::math::linearalgebra::setelem C 1 1 $c11

        	#Returns Complex Matrix C (product of A and B)
        	return $C
	}

	#Define Q range
	if {$man_q} {
		set Qgridsize [expr ($Qmax-$Qmin)/$Qngrid]	
		set q_list {}
		for {set q_inx 0} {$q_inx <= $Qngrid} {incr q_inx} {
			set qman [expr $Qmin+$q_inx*$Qgridsize]
			lappend q_list $qman
		}
	} elseif {$exp_q} {
		#Read experimental data
                set filein [open "$expfile" r]
                set datasz [file size "$expfile"]
                set datain [split [read $filein $datasz] \n]
                set qEin {}
                set rEin {}
		set REin {}
                set eEup {}
                set eEdown {}
                for {set point 0} {$point < [expr [llength $datain]-1]} {incr point} {
                        set q_value [lindex [lindex $datain $point] 0]
                        set r_value [lindex [lindex $datain $point] 1]
                        set e_value [lindex [lindex $datain $point] 2]
                        set eup_value [expr $r_value + $e_value]
                        set edown_value_temp [expr $r_value - $e_value]
                        if {$edown_value_temp <= 0} {
                                set edown_value $r_value
                        } else {
                                set edown_value $edown_value_temp
                        }
#                        puts "$q_value $r_value $eup_value $edown_value"

                        lappend qEin $q_value
			lappend eEin $e_value
			lappend REin $r_value
                        lappend rEin [expr log10($r_value)]
                        lappend eEup [expr log10($eup_value)]
                        lappend eEdown [expr log10($edown_value)]
                }
                close $filein
		#Define q range from experimental data points
		set q_list $qEin
	
		#Gaussian filter for resolution smearing

		set outfile [open Rconv.dat w]
		set gaussdev [open gaussdev.dat w]

		set gaussnumpts 5
		set gauss_fwhm_fr 0.1

		set M $gaussnumpts
		set half_gauss [expr int(floor($M/2.0))]
		set Qpad_up_pre [lrange $qEin 0 $half_gauss]
		set Qpad_down_pre [lrange $qEin [expr [llength $qEin] - $half_gauss - 1] [expr [llength $qEin]]]
#		set Rpad_up [lrange $REin 0 $half_gauss]
#		set Rpad_down [lrange $REin [expr [llength $REin] - $half_gauss - 1] [expr [llength $REin]]]
		set first_q [lindex $Qpad_up_pre 0]
		set last_q [lindex $Qpad_down_pre [expr [llength $Qpad_down_pre] - 1]]
		set Qpad_up_inv {}
		set Rpad_up {}
		for {set x 1} {$x<[llength $Qpad_up_pre]} {incr x} {
			set pre_q [lindex $Qpad_up_pre $x]
			lappend Qpad_up_inv [expr $first_q - ($pre_q - $first_q)]
			lappend Rpad_up 0
		}
		set Qpad_up [lreverse $Qpad_up_inv]

		set Qpad_down_inv {}
		set Rpad_down {}
		for {set x 0} {$x<[expr [llength $Qpad_down_pre] - 1]} {incr x} {
			set post_q [lindex $Qpad_down_pre $x]
			lappend Qpad_down_inv [expr $last_q + ($last_q - $post_q)]
			lappend Rpad_down 0
		}
		set Qpad_down [lreverse $Qpad_down_inv]

		set Qin [concat $Qpad_up $qEin $Qpad_down]
		set R2blur [concat $Rpad_up $REin $Rpad_down]

		set Q_fwhm {}
		foreach q $Qin {
			lappend Q_fwhm [list $q [expr $q*$gauss_fwhm_fr]]
		}	


		# Procedure to evaluate gaussian function
		proc gauss_eval {gaussavg gaussfwhm input} {
    			set sd [expr $gaussfwhm/(2*sqrt(2*log(2)))]
    			set pi 3.1415926535897931
    			set G [expr (1/sqrt(2*$pi*pow($sd,2)))*exp(-(pow(($input-$gaussavg),2)/(2*pow($sd,2))))]
    		return $G
		}

		set last_q [expr [llength $Qin] - $M]

		set Rblur_list {}
		set logRblur_list {}
		for {set q 0} {$q <= $last_q} {incr q} {
			set subQ_fwhm [lrange $Q_fwhm $q [expr $q + $M - 1]]
			set subR [lrange $R2blur $q [expr $q + $M - 1]]
			set gauss_avg [lindex [lindex $subQ_fwhm $half_gauss] 0]
			set gauss_fwhm [lindex [lindex $subQ_fwhm $half_gauss] 1]
			set sd [expr $gauss_fwhm/(2*sqrt(2*log(2)))]

			set g_sum 0
			foreach s_qfwhm $subQ_fwhm {
				set sq [lindex $s_qfwhm 0]
				set g [gauss_eval $gauss_avg $gauss_fwhm $sq]
				set g_sum [expr $g_sum + $g]
			}

			set Rconv 0
			foreach s_qfwhm $subQ_fwhm sr $subR {
				set sq [lindex $s_qfwhm 0]
				set g [gauss_eval $gauss_avg $gauss_fwhm $sq]
				set Rconv [expr $Rconv + $g*$sr]
				puts $gaussdev [format "%.10f" [expr ($gauss_avg-$sq)/$sd]]
			}
			set Rblur [expr $Rconv/$g_sum]
			puts $outfile "$gauss_avg $Rblur"
			lappend Rblur_list $Rblur
			lappend logRblur_list [expr log10($Rblur)]
		}
		close $outfile
		close $gaussdev
	}	


	if {$sim_sld} {
#       	set Qgridsize [expr ($Qmax-$Qmin)/$Qngrid]
       	set pi 3.1415926535897931
       	set ref_vs_q [open "ref_vs_q.dat" w]
       	set refq4_vs_q [open "refq4_vs_q.dat" w]

	if {$SLDmemory} {
		#Remove fronting and backing points from simSLDlist to compare with experimental supported bilayers (using in memory data)
		set choped_simSLDlist [lrange $simSLDlist $fronting [expr [llength $simSLDlist]-$backing-1]]
	} elseif {$SLDload} {
		#Read NSLD and thicknes previously saved to disk
		set savedNSLDin [open "$savedNSLDfile" r]
		set sNSLDsize [file size "$savedNSLDfile"]
                set sNSLDdata [split [read $savedNSLDin $sNSLDsize] \n]
		set simSLDlist_load {}
		set zpoint_list {}
		for {set sNSLDp 0} {$sNSLDp < [expr [llength $sNSLDdata]-1]} {incr sNSLDp} {
			set zvalue [lindex [lindex $sNSLDdata $sNSLDp] 0]
			set sNLSDvalue [lindex [lindex $sNSLDdata $sNSLDp] 1]
			lappend zpoint_list $zvalue 
			lappend simSLDlist_load $sNLSDvalue
		}
		close $savedNSLDin
#		puts $simSLDlist_load

		#Remove fronting and backing points from simSLDlist to compare with experimental supported bilayers (using loaded data)
		set choped_simSLDlist [lrange $simSLDlist_load $fronting [expr [llength $simSLDlist_load]-$backing-1]]
		#Set thickness for NSLD from MD to be loaded when reading from file
		set thick [lindex $zpoint_list 1]
#		puts $thick 

	}

       	#Generate SLD list. (Add SLD of Surf1 to the simSLD list (SLD1))
       	set prelist [list [expr $SLD1]]
	set prelist_s [list [expr $SLD2]]  ;# A: Extra backing layer between surface and substrate i.e.: more solvent
	set postlist [list [expr $SLDlast]]
	set SLD_list [concat $prelist $prelist_s $choped_simSLDlist $postlist] ;#M: Extra backing layer between surface and substrate i.e.: more solvent
       	set num_s_list [llength $SLD_list]

       	#Define parameters of layer=0 (Surf0)
       	set s(0) [expr $SLD0*1.0e-6]
       	set b(0) [::math::complexnumbers::complex 0 0]

       	#Define parameters for layer=1 (Surf1)
       	set d(1) [::math::complexnumbers::complex $thick1 0]
       	set sigma(1) $rough1

	#Define parameters for layer=2 (space between surface and solute)  ;# A: Extra backing layer between surface and substrate i.e.: more solvent
        set d(2) [::math::complexnumbers::complex $thick2 0]               ;# A: Extra backing layer between surface and substrate i.e.: more solvent
        set sigma(2) $rough2                                               ;# A: Extra backing layer between surface and substrate i.e.: more solvent

       	#Generate thickness and roughness lists
       	for {set n 3} {$n<$num_s_list} {incr n} {   ;#M: Extra backing layer between surface and substrate i.e.: more solvent
               	set sigma($n) $rough
               	set d($n) [::math::complexnumbers::complex $thick 0]
       	}

#	set sigma(3) 4.0  ;# Temporary fix to define the roughness of layer 3 separately than the rest of the MD layers

       	#Define parameters for layer=n+1 (solvent)      
	set sigma($num_s_list) $roughlast
       	set d($num_s_list) [::math::complexnumbers::complex 0 0] ;# Added for completeness. Could be any value.


       	#Standard Output printing:

       	puts "\n"
       	puts "#################        Reflectivity Calculation       ##################\n"
       	puts "##########################################################################"
       	puts "#                                                                        #"
       	puts "#          ki\\  / kf                                                     #"
       	puts "#             \\/                 --> layer 0: Surf0                      #"
       	puts "#       ------------------------                                         #"
       	puts "#                                --> layer 1: Surf1                      #"
       	puts "#       ------------------------             _                           #"
       	puts "#                                    .        |                          #"
       	puts "#       ::::::::::::::::::::::::     .        |                          #"
       	puts "#                                    .        | n-1 layers of membrane   #"
       	puts "#       ------------------------              |                          #"
       	puts "#                                --> layer n  |                          #"
       	puts "#       ------------------------             _|                          #"
       	puts "#                                                                        #"
       	puts "#                                --> layer n+1: solvent                  #"
       	puts "#                                                                        #"
       	puts "##########################################################################\n"

       	puts "------------------------    Input Parameters    -------------------------\n"
       	puts "Layer   0 - SLD: $SLD0"
       	puts "Layer   1 - SLD: $SLD1, Thickness: $thick1 and Roughness: $rough1"
	puts "Layer   2 - SLD: $SLD2, Thickness: $thick2 and Roughness: $rough2"
	puts "Layer n+1 - SLD: $SLDlast and Roughness: $roughlast"
       	puts "Bin size of simulated NSLD: [format "%.3f" $thick]"
       	puts "Number of NSLD points: $num_s_list"
	puts "Roughness (sigma in Nevot&Croce error function): $rough"
       	puts "Backgound: $bcgr"
       	puts "Q range(A-1): $Qmin - $Qmax"
       	puts "------------------------------   Running  ------------------------------"

#	set q_list {}
	set logR_list {}
	set R_list {}
	set RQ4_list {}
       	#Loop over Q values
	foreach q $q_list {
 #              	set q [expr $Qmin+$q_inx*$Qgridsize]
#		lappend q_list $q
               	set k(0) [::math::complexnumbers::complex [expr $q/2] 0]

               	#Initialise resultant matrix
               	set Rm [::math::linearalgebra::mkMatrix 2 2 0]
               	::math::linearalgebra::setelem Rm 0 0 [::math::complexnumbers::complex 1 0]
               	::math::linearalgebra::setelem Rm 0 1 [::math::complexnumbers::complex 0 0]
               	::math::linearalgebra::setelem Rm 1 0 [::math::complexnumbers::complex 0 0]
               	::math::linearalgebra::setelem Rm 1 1 [::math::complexnumbers::complex 1 0]
               	for {set i 1} {$i<=$num_s_list} {incr i} {

                       	set j [expr $i-1]
                       	#Define SLD(i)
                       	set s($i) [expr [lindex $SLD_list $j]*1.0e-6]
                       	#Definition of refraction k(i)
                       	#Determine sign of sqrt argument (SQRT_A)
                       	set SQRT_A [expr (pow([::math::complexnumbers::real $k(0)],2))-4*$pi*($s($i)-$s(0))]
                       	if {$SQRT_A >= 0} {
                       	        set r_part [expr {sqrt($SQRT_A)}]
				set i_part 0
                       	} else {
                       	        set r_part 0
                       	        set i_part [expr {sqrt(-$SQRT_A)}]
                       	}

                       	#Define k(i) as complex number
                       	set k($i) [::math::complexnumbers::complex $r_part $i_part]
	
                        #Define phase factor b(i-1)
                        set b($i) [::math::complexnumbers::* $k($i) $d($i)]

                       	#Define define roughness corrected Fresnel reflection r(i-1)
                       	#TERMS DEFINITIONS
                       	#r=K_sub_over_K_sum*exp_PREXP_by_Kprod_by_R2
                       	#K_sub_over_K_sum = K_sub/K_sum 
                       	#exp_PREXP_by_Kprod_by_R2=exp(PREXP_by_Kprod_by_R2)
                       	#K_sub=$k($j)-$k($i)
                       	#K_sum=$k($j)+$k($i)
                       	#PREXP_by_Kprod_by_R2=PREXP*Kprod*R2
                       	#PREXP=-2
                       	#Kprod=$k($j)*$k($i)
                       	#R2=pow($sigma,2)
                       	#set r [expr {(($k($j)-$k($i))/($k($j)+$k($i)))*exp(-2*$k($j)*$k($i)*(pow($sigma,2)))}]
                       	set K_sub [::math::complexnumbers::- $k($j) $k($i)]
                       	set K_sum [::math::complexnumbers::+ $k($j) $k($i)]
                       	set PREEXP [::math::complexnumbers::complex -2 0]
                       	set Kprod [::math::complexnumbers::* $k($j) $k($i)]
#                      	set R2 [::math::complexnumbers::pow $sigma [math::complexnumbers::complex 2 0]]
                       	set R2 [::math::complexnumbers::complex [expr pow($sigma($i),2)] 0]
                       	set K_sub_over_K_sum [::math::complexnumbers::/ $K_sub $K_sum]
                       	set PREXP_by_Kprod_by_R2 [::math::complexnumbers::* [::math::complexnumbers::* $PREEXP $Kprod] $R2]
                       	set exp_PREXP_by_Kprod_by_R2 [::math::complexnumbers::exp $PREXP_by_Kprod_by_R2]
                       	#Roughness corrected Fresnel from terms definitions
                       	set r [::math::complexnumbers::* $K_sub_over_K_sum $exp_PREXP_by_Kprod_by_R2]

                        #Define characteristic matrix C(i-1)
                        set ib [::math::complexnumbers::* [::math::complexnumbers::complex 0 1] $b($j)]
#                       set bneg [::math::complexnumbers::* [::math::complexnumbers::complex -1 0] $b($j)]
			set ibneg [::math::complexnumbers::* [::math::complexnumbers::complex -1 0] $ib]
#                       set exp_b [::math::complexnumbers::exp $b($j)]
#                       set exp_bneg [::math::complexnumbers::exp $bneg]
                       	set exp_ib [::math::complexnumbers::exp $ib]
                       	set exp_ibneg [::math::complexnumbers::exp $ibneg]
                       	set c_11 $exp_ib
                       	set c_12 [::math::complexnumbers::* $r $exp_ib]
                       	set c_21 [::math::complexnumbers::* $r $exp_ibneg]
                       	set c_22 $exp_ibneg
	
                        #Example to test the performance of the complex matrix calculation (Result: ((7 10) (15 22)))
                        #set c_11 [::math::complexnumbers::complex 1 0]
                        #set c_12 [::math::complexnumbers::complex 2 0]
                        #set c_21 [::math::complexnumbers::complex 3 0]
                        #set c_22 [::math::complexnumbers::complex 4 0]
                        set C($i) [::math::linearalgebra::mkMatrix 2 2 0]
                        ::math::linearalgebra::setelem C($i) 0 0 $c_11
                        ::math::linearalgebra::setelem C($i) 0 1 $c_12
                        ::math::linearalgebra::setelem C($i) 1 0 $c_21
                        ::math::linearalgebra::setelem C($i) 1 1 $c_22

                        #Define resultant matrix
                        set Rm [mult_cplx_mat $Rm $C($i)]
               	}
               	#Calculate reflectivity R(q)
               	set Rm_11 [::math::linearalgebra::getelem $Rm 0 0]
               	set Rm_21 [::math::linearalgebra::getelem $Rm 1 0]
               	set Rm_11_conj [::math::complexnumbers::conj $Rm_11]
               	set Rm_21_conj [::math::complexnumbers::conj $Rm_21]
               	set Rm11_by_Rm11conj [::math::complexnumbers::* $Rm_11 $Rm_11_conj]
               	set Rm21_by_Rm21conj [::math::complexnumbers::* $Rm_21 $Rm_21_conj]
#               	set R($q) [expr ([::math::complexnumbers::real $Rm21_by_Rm21conj]/[::math::complexnumbers::real $Rm11_by_Rm11conj])+$bcgr ]
		set R($q) [expr (([::math::complexnumbers::real $Rm21_by_Rm21conj]/[::math::complexnumbers::real $Rm11_by_Rm11conj])+$bcgr)*$scl ]
		
		lappend R_list $R($q)
		lappend logR_list [expr log10($R($q))]
                set Rq4($q) [expr $R($q)*pow($q,4)]
		lappend RQ4_list $Rq4($q)
                puts $ref_vs_q [format "%.5f %.17f" $q $R($q)]
                puts $refq4_vs_q [format "%.5f %.17f" $q $Rq4($q)]
        }
	close $ref_vs_q
	close $refq4_vs_q
	if {$man_q} {
		multiplot -x $q_list -y $logR_list -title "Reflectivity" -linewidth 2.0 -marker point -xlabel "Q (A^-1)" -ylabel "    log R(Q)" -plot
		multiplot -x $q_list -y $RQ4_list -title "Reflectivity" -linewidth 2.0 -marker point -xlabel "Q (A^-1)" -ylabel "    R(Q)*Q^4" -plot	
	} elseif {$exp_q} {
		#Compute chi squared
		set sum_chi 0	
		set sum_chi_blur 0
		set nr_qpoints [llength $R_list]
		puts [llength $REin]
		puts [llength $Rblur_list]

		for {set qpoint 0} {$qpoint < $nr_qpoints} {incr qpoint} {	
			set R_md [lindex $R_list $qpoint]
			set R_exp_blur [lindex $Rblur_list $qpoint]
			set R_exp [lindex $REin $qpoint]
			set E_exp [lindex $eEin $qpoint]

#			set log_R_md [lindex $logR_list $qpoint]
#			set log_R_exp [lindex $rEin $qpoint]
#			set log_R_exp_blur [lindex $logRblur_list $qpoint]
#			set log_E_exp [expr log10(1 + pow($E_exp,2)/pow($R_exp,2))]
#			set chi_temp [expr (pow(($log_R_md-$log_R_exp),2)/$log_E_exp)]
#			set chi_temp_blur [expr (pow(($log_R_md-$log_R_exp_blur),2)/$log_E_exp)]
			
			set chi_temp [expr (pow(($R_md-$R_exp),2)/pow($E_exp,2))]
			set chi_temp_blur [expr (pow(($R_md-$R_exp_blur),2)/pow($E_exp,2))]
			set sum_chi [expr $sum_chi + $chi_temp]
			set sum_chi_blur [expr $sum_chi_blur + $chi_temp_blur]
		}
#		set chi2 [expr sqrt($sum_chi)/sqrt($nr_qpoints-1)]
		set chi2 [expr ($sum_chi)/($nr_qpoints-1)]
		set chi2_blur [expr ($sum_chi_blur)/($nr_qpoints-1)]

		#Plot data
		set ph [multiplot -x $qEin -y $rEin -title "Reflectivity" -linewidth 2.0 -marker square -radius 4.0 -fillcolor "black" -xlabel "Q (A^-1)" -ylabel "    log R(Q)" -legend "Exp."]
        	$ph add $qEin $eEup -dash "-" -linewidth 1.4 -linecolor "red" -legend "Exp. Error"
        	$ph add $qEin $eEdown -dash "-" -linewidth 1.4 -linecolor "red"
		$ph add $q_list $logR_list -linewidth 2.0 -linecolor "blue" -marker circle -radius 1.0 -fillcolor "blue" -legend "MD (chi2=[format "%.5f" $chi2])"
		$ph add $q_list $logRblur_list -linewidth 2.0 -linecolor "green" -marker square -radius 4.0 -fillcolor "green" -legend "MD-smeared (chi2=[format "%.5f" $chi2_blur])"
        	$ph replot
	}

	} elseif {$man_sld} {
	puts "\n"
	puts "#########################################################"
	puts "#######    THIS FUNCTION IS NOT READY YET        ########"
	puts "#######   PLEASE WAIT UNTIL NEXT VERSION :)      ########"
	puts "#########################################################"
	puts "\n"
	}
}

#callback for VMD menu entry
proc md2nsld_tk {} {
	NSLD::nsldgui
	return $NSLD::w
}
